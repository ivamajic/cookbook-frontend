/* eslint-disable import/no-cycle */
/* eslint-disable func-names */
import axios from "axios";
import store from "../../store";
import { authUrl } from "../../env";

export const login = () => ({
  type: "LOGIN",
});

export const loggedIn = user => ({
  type: "LOGGED_IN",
  payload: {
    user,
  },
});

export const logout = () => ({
  type: "LOGOUT",
});

export const loggedOut = auth => ({
  type: "LOGGED_OUT",
  payload: {
    auth,
  },
});

export const register = () => ({
  type: "REGISTER",
});

export const registered = user => ({
  type: "REGISTERED",
  payload: {
    user,
  },
});


export const dispatchLogout = (token) => {
  store.dispatch(logout());
  return function (dispatch) {
    axios(
      {
        method: "get",
        url: `${authUrl}/logout`,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    )
      .then((response) => {
        dispatch(loggedOut(response.data));
      });
  };
};

export const dispatchLogin = (email, password) => {
  store.dispatch(login());
  return function (dispatch) {
    axios(
      {
        method: "post",
        url: `${authUrl}/login`,
        data: {
          email,
          password,
        },
      },
    )
      .then((response) => {
        dispatch(loggedIn(response.data));
      });
  };
};

export const dispatchRegister = (email, password, name) => {
  store.dispatch(register());
  return function (dispatch) {
    axios(
      {
        method: "post",
        url: `${authUrl}/register`,
        data: {
          email,
          password,
          name,
        },
      },
    )
      .then((response) => {
        dispatch(registered(response.data));
      });
  };
};
