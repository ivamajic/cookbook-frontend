/* eslint-disable import/no-cycle */
/* eslint-disable func-names */
import axios from "axios";
import store from "../../store";
import { recipesUrl } from "../../env";

export const fetchRecipes = () => ({
  type: "FETCH_RECIPES",
});

export const recipesFetched = recipes => ({
  type: "RECIPES_FETCHED",
  payload: {
    recipes,
  },
});


export const fetchRecipeById = id => ({
  type: "FETCH_RECIPE",
  payload: {
    id,
  },
});

export const recipeFetched = selectedRecipe => ({
  type: "RECIPE_FETCHED",
  payload: {
    selectedRecipe,
  },
});

export const fetchMyRecipes = () => ({
  type: "FETCH_MY_RECIPES",
});

export const myRecipesFetched = myRecipes => ({
  type: "MY_RECIPES_FETCHED",
  payload: {
    myRecipes,
  },
});

export const createRecipe = () => ({
  type: "CREATE_RECIPE",
});

export const recipeCreated = recipe => ({
  type: "RECIPE_CREATED",
  payload: {
    recipe,
  },
});

export const editRecipe = () => ({
  type: "EDIT_RECIPE",
});

export const recipeEdited = recipe => ({
  type: "RECIPE_EDITED",
  payload: {
    recipe,
  },
});

export const deleteRecipe = () => ({
  type: "DELETE_RECIPE",
});

export const recipeDeleted = () => ({
  type: "RECIPE_DELETED",
});

export const dispatchFetchRecipes = () => {
  store.dispatch(fetchRecipes());
  return function (dispatch) {
    axios.get(`${recipesUrl}`)
      .then((response) => {
        dispatch(recipesFetched(response.data));
      });
  };
};

export const dispatchFetchMyRecipes = (user) => {
  store.dispatch(fetchMyRecipes());
  return function (dispatch) {
    axios(
      {
        method: "get",
        url: `${recipesUrl}/manage/${user.id}/${user.role}`,
        headers: {
          Authorization: `Bearer ${user.accessToken}`,
        },
      },
    )
      .then((response) => {
        dispatch(myRecipesFetched(response.data));
      });
  };
};

export const dispatchFetchRecipeById = (id) => {
  store.dispatch(fetchRecipeById());
  return function (dispatch) {
    axios.get(`${recipesUrl}/${id}`)
      .then((response) => {
        dispatch(recipeFetched(response.data));
      });
  };
};

export const dispatchDeleteRecipe = (id) => {
  store.dispatch(deleteRecipe());
  return function (dispatch) {
    axios.delete(`${recipesUrl}/${id}`)
      .then(() => {
        dispatch(recipeDeleted());
      });
  };
};

export const dispatchCreateRecipe = (
  title,
  description,
  imageUrl,
  kcal,
  protein,
  fat,
  carbohydrates,
  category,
  userId,
  ingredients,
  instructions,
  tags,
  token,
) => {
  store.dispatch(createRecipe());
  return function (dispatch) {
    axios(
      {
        method: "post",
        url: `${recipesUrl}`,
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data: {
          title,
          description,
          imageUrl,
          kcal,
          protein,
          fat,
          carbohydrates,
          category,
          user_id: userId,
          ingredients,
          instructions,
          tags,
        },
      },
    )
      .then((response) => {
        dispatch(recipeCreated(response.data));
      });
  };
};

export const dispatchEditRecipe = (
  title,
  description,
  imageUrl,
  kcal,
  protein,
  fat,
  carbohydrates,
  category,
  ingredients,
  instructions,
  tags,
  token,
  recipeId,
) => {
  store.dispatch(editRecipe());
  return function (dispatch) {
    axios(
      {
        method: "put",
        url: `${recipesUrl}/${recipeId}`,
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data: {
          title,
          description,
          imageUrl,
          kcal,
          protein,
          fat,
          carbohydrates,
          category,
          ingredients,
          instructions,
          tags,
        },
      },
    )
      .then((response) => {
        dispatch(recipeEdited(response.data));
      });
  };
};
