const initState = {
  user: {},
  loggedIn: false,
};

const auth = (state = initState, action) => {
  switch (action.type) {
    case "LOGGED_IN": {
      return {
        ...state,
        user: action.payload.user,
        loggedIn: true,
      };
    }
    case "LOGGED_OUT": {
      return {
        ...state,
        user: {},
        loggedIn: false,
      };
    }
    case "REGISTERED": {
      return {
        ...state,
        user: action.payload.user,
        loggedIn: true,
      };
    }
    default: {
      return state;
    }
  }
};

export default auth;
