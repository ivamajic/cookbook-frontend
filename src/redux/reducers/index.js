import { combineReducers } from "redux";
import recipes from "./recipes";
import auth from "./auth";
import { baseUrl } from "../../env";

const reducers = combineReducers({
  recipes,
  auth,
});

const rootReducer = (state, action) => {
  if (action.type === "LOGGED_OUT") {
    state = undefined;
    window.location.assign(
      `${baseUrl}login`,
    );
  } else if (
    action.type === "RECIPE_DELETED"
  || action.type === "RECIPE_CREATED"
  || action.type === "RECIPE_EDITED"
  || action.type === "LOGGED_IN"
  || action.type === "REGISTERED"
  ) {
    window.location.assign(
      `${baseUrl}recipes`,
    );
  }
  return reducers(state, action);
};

export default rootReducer;
