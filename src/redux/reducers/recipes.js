const initState = {
  loading: false,
  recipes: [],
  myRecipes: [],
  selectedRecipe: {},
};

const recipes = (state = initState, action) => {
  switch (action.type) {
    case "RECIPES_FETCHED": {
      return {
        ...state,
        loading: false,
        recipes: action.payload.recipes,
      };
    }
    case "MY_RECIPES_FETCHED": {
      return {
        ...state,
        myRecipes: action.payload.myRecipes,
      };
    }
    case "RECIPE_FETCHED": {
      return {
        ...state,
        loading: false,
        selectedRecipe: action.payload.selectedRecipe,
      };
    }
    case "RECIPE_EDITED": {
      return {
        ...state,
        recipe: action.payload.recipe,
      };
    }
    default: {
      return state;
    }
  }
};

export default recipes;
