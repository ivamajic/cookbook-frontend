const baseApiUrl = "http://127.0.0.1:8000/api/";

export const baseUrl = "http://localhost:4200/";

export const authUrl = `${baseApiUrl}auth`;
export const recipesUrl = `${baseApiUrl}recipes`;
