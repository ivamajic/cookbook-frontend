import React, { Component } from "react";
import { connect } from "react-redux";
import { dispatchLogin } from "../../../redux/actions";

import LoginPresenter from "../LoginPresenter/LoginPresenter";

class LoginContainer extends Component {
  constructor(props) {
    super(props);
    this.login = this.login.bind(this);
    this.changeEmailInput = this.changeEmailInput.bind(this);
    this.changePasswordInput = this.changePasswordInput.bind(this);
    this.state = {
      email: "",
      password: "",
    };
  }

  changeEmailInput = (value) => {
    this.setState({
      email: value,
    });
  }

  changePasswordInput = (value) => {
    this.setState({
      password: value,
    });
  }

  login = () => {
    this.props.dispatch(dispatchLogin(this.state.email, this.state.password));
  }

  render() {
    return (
      <LoginPresenter
        email={this.state.email}
        password={this.state.password}
        login={this.login}
        changeEmailInput={this.changeEmailInput}
        changePasswordInput={this.changePasswordInput}
      />
    );
  }
}

const mapStateToProps = state => ({
  loggedUser: state.auth.user,
});

export default connect(mapStateToProps)(LoginContainer);
