import React, { Component } from "react";

import "./LoginPresenter.scss";

class LoginPresenter extends Component {
  render() {
    return (
      <div className="login-container">
        <div className="login-container__header">
          <div className="login-container__header__title">Login</div>
        </div>
        <div className="login-container__inside">
          <div className="login-container__inside__row">
            <input
              type="text"
              placeholder="E-mail"
              value={this.props.email}
              onChange={e => this.props.changeEmailInput(e.target.value)}
            />
          </div>
          <div className="login-container__inside__row">
            <input
              type="password"
              placeholder="Password"
              value={this.props.password}
              onChange={e => this.props.changePasswordInput(e.target.value)}
            />
          </div>
          <div className="login-container__inside__row">
            <button type="submit" onClick={this.props.login}>Login</button>
          </div>
        </div>
      </div>
    );
  }
}

export default LoginPresenter;
