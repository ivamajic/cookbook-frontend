import React, { Component } from "react";
import { connect } from "react-redux";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Container } from "reactstrap";

import Routes from "./Routes";

class App extends Component {
  render() {
    return (
      <Router>
        <Container fluid>
          <Switch>
            <Route component={Routes} />
          </Switch>
        </Container>
      </Router>
    );
  }
}

const mapStateToProps = state => ({
  loggedUser: state.auth.user,
});

export default connect(mapStateToProps)(App);
