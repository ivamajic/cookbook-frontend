import React from "react";
import { Switch, Route } from "react-router-dom";
import { Row } from "reactstrap";

import NavigationContainer from "./Navigation/NavigationContainer/NavigationContainer";
import HomeContainer from "./Home/HomeContainer/HomeContainer";
import RecipesContainer from "./Recipes/RecipesContainer/RecipesContainer";
import RecipeContainer from "./Recipe/RecipeContainer/RecipeContainer";
import LoginContainer from "./Login/LoginContainer/LoginContainer";
import RegisterContainer from "./Register/RegisterContainer/RegisterContainer";
import NewRecipeContainer from "./NewRecipe/NewRecipeContainer/NewRecipeContainer";

const Routes = () => (
  <Row>
    <NavigationContainer />
    <Switch>
      <Route exact path="/" component={HomeContainer} />
      <Route path="/login" component={LoginContainer} />
      <Route path="/recipes" component={RecipesContainer} />
      <Route path="/managerecipes" component={RecipesContainer} />
      <Route path="/recipe/:id" component={RecipeContainer} />
      <Route path="/register" component={RegisterContainer} />
      <Route path="/recipeform" component={NewRecipeContainer} />
      <Route path="/editrecipe/:id" component={NewRecipeContainer} />
    </Switch>
  </Row>
);

export default Routes;
