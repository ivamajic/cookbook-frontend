import React, { Component } from "react";

import "./NavigationPresenter.scss";

class NavigationPresenter extends Component {
  render() {
    const { loggedUser, loggedIn } = this.props;
    return (
      <div className="navigation">
        { loggedIn
          ? (
            <div className="navigation__header">
              Hi,
              {" "}
              {loggedUser.name}
              !
            </div>
          )
          : null
        }
        <div className="navigation__links">
          <div className="navigation__links__link"><a href="/recipes">Recipes</a></div>
          {
            loggedIn
              ? (
                <div className="navigation__links__link"><a href="/recipeform">New recipe</a></div>
              ) : null
          }
        </div>
        <div className="navigation__footer">
          { !loggedIn
            ? (
              <div>
                <div className="navigation__footer__link"><a href="/login">Login</a></div>
                <div className="navigation__footer__link"><a href="/register">Register</a></div>
              </div>
            )
            : null
          }
          { loggedIn
            ? (
              <div>
                <div className="navigation__footer__link"><a href="/managerecipes">Manage Recipes</a></div>
                <div className="navigation__footer__link"><button type="button" onClick={this.props.logout}>Logout</button></div>
              </div>
            )
            : null
          }
        </div>
      </div>
    );
  }
}

export default NavigationPresenter;
