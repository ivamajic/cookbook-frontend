import React, { Component } from "react";
import { connect } from "react-redux";
import { dispatchLogout } from "../../../redux/actions";

import NavigationPresenter from "../NavigationPresenter/NavigationPresenter";

class NavigationContainer extends Component {
  constructor(props) {
    super(props);
    this.logout = this.logout.bind(this);
  }

  logout = () => {
    this.props.dispatch(dispatchLogout(this.props.loggedUser.accessToken));
  }

  render() {
    return (
      <NavigationPresenter
        logout={this.logout}
        loggedUser={this.props.loggedUser}
        loggedIn={this.props.loggedIn}
      />
    );
  }
}

const mapStateToProps = state => ({
  loggedUser: state.auth.user,
  loggedIn: state.auth.loggedIn,
});

export default connect(mapStateToProps)(NavigationContainer);
