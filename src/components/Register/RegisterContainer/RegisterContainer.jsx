import React, { Component } from "react";
import { connect } from "react-redux";

import RegisterPresenter from "../RegisterPresenter/RegisterPresenter";
import { dispatchRegister } from "../../../redux/actions";

class RegisterContainer extends Component {
  constructor(props) {
    super(props);
    this.register = this.register.bind(this);
    this.changeEmailInput = this.changeEmailInput.bind(this);
    this.changePasswordInput = this.changePasswordInput.bind(this);
    this.changeNameInput = this.changeNameInput.bind(this);
    this.state = {
      name: "",
      email: "",
      password: "",
    };
  }

  changeNameInput = (value) => {
    this.setState({
      name: value,
    });
  }

    changeEmailInput = (value) => {
      this.setState({
        email: value,
      });
    }

    changePasswordInput = (value) => {
      this.setState({
        password: value,
      });
    }

    register = () => {
      this.props.dispatch(dispatchRegister(this.state.email, this.state.password, this.state.name));
    }

    render() {
      return (
        <RegisterPresenter
          email={this.state.email}
          password={this.state.password}
          name={this.state.name}
          register={this.register}
          changeEmailInput={this.changeEmailInput}
          changePasswordInput={this.changePasswordInput}
          changeNameInput={this.changeNameInput}
        />
      );
    }
}

const mapStateToProps = state => ({
  loggedUser: state.auth.user,
});

export default connect(mapStateToProps)(RegisterContainer);
