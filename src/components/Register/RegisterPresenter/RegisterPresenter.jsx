import React, { Component } from "react";

import "./RegisterPresenter.scss";

class RegisterPresenter extends Component {
  render() {
    return (
      <div className="register-container">
        <div className="register-container__header">
          <div className="register-container__header__title">Register</div>
        </div>
        <div className="register-container__inside">
          <div className="register-container__inside__row">
            <input
              type="text"
              placeholder="Name"
              value={this.props.name}
              onChange={e => this.props.changeNameInput(e.target.value)}
            />
          </div>
          <div className="register-container__inside__row">
            <input
              type="email"
              placeholder="E-mail"
              value={this.props.email}
              onChange={e => this.props.changeEmailInput(e.target.value)}
            />
          </div>
          <div className="register-container__inside__row">
            <input
              type="password"
              placeholder="Password"
              value={this.props.password}
              onChange={e => this.props.changePasswordInput(e.target.value)}
            />
          </div>
          <div className="register-container__inside__row">
            <button type="submit" onClick={this.props.register}>Register</button>
          </div>
        </div>
      </div>
    );
  }
}

export default RegisterPresenter;
