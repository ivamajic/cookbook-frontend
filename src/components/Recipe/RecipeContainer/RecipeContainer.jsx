import React, { Component } from "react";
import { connect } from "react-redux";
import { dispatchFetchRecipeById, dispatchDeleteRecipe } from "../../../redux/actions";

import RecipePresenter from "../RecipePresenter/RecipePresenter";

class RecipeContainer extends Component {
  constructor(props) {
    super(props);
    this.deleteRecipe = this.deleteRecipe.bind(this);
  }

  componentDidMount() {
    this.props.dispatch(dispatchFetchRecipeById(this.props.match.params.id));
  }

  deleteRecipe = (id) => {
    this.props.dispatch(dispatchDeleteRecipe(id));
  }

  render() {
    return (
      <RecipePresenter
        recipe={this.props.selectedRecipe}
        deleteRecipe={this.deleteRecipe}
        canEdit={this.props.loggedUser.role === "admin" || this.props.selectedRecipe.user_id === this.props.loggedUser.id}
      />
    );
  }
}

const mapStateToProps = state => ({
  selectedRecipe: state.recipes.selectedRecipe,
  loggedUser: state.auth.user,
});

export default connect(mapStateToProps)(RecipeContainer);
