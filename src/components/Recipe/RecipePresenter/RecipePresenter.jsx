import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash, faEdit } from "@fortawesome/free-solid-svg-icons";

import "./RecipePresenter.scss";

class RecipePresenter extends Component {
  render() {
    const { recipe } = this.props;
    return (
      <div className="recipe-container">
        <div className="recipe-container__header">
          <div className="recipe-container__header__title">{recipe ? recipe.title : null}</div>
          {
            this.props.canEdit ? (
              <div className="recipe-container__header__icons">
                <a href={`/editrecipe/${recipe.id}`}><FontAwesomeIcon icon={faEdit} /></a>
                <FontAwesomeIcon icon={faTrash} onClick={() => this.props.deleteRecipe(recipe.id)} />
              </div>
            ) : null
          }
        </div>
        <div className="recipe-container__inside">
          <div className="recipe-container__inside__tags">
            <div className="recipe-container__inside__tags__tag">
                Category:
              {" "}
              <span style={{ textTransform: "capitalize" }}>{recipe ? recipe.category : null}</span>
            </div>
            <div className="recipe-container__inside__tags__tag">
                    Tags:
              {recipe && recipe.tags ? recipe.tags.map(tag => (<span key={tag.id} style={{ marginLeft: "5px" }}>{tag.tag}</span>)) : "none"}
            </div>
          </div>
          <div className="recipe-container__inside__image">
            <img src={recipe ? recipe.imageUrl : null} alt="" />
          </div>
          <div className="recipe-container__inside__description">
            {recipe ? recipe.description : null}
          </div>
          <div className="recipe-container__inside__table">
            <table>
              <thead>
                <tr>
                  <th>Energy (kcal)</th>
                  <th>Fats (g)</th>
                  <th>Protein (g)</th>
                  <th>Carbohydrates (g)</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{recipe ? recipe.kcal : null}</td>
                  <td>{recipe ? recipe.fat : null}</td>
                  <td>{recipe ? recipe.protein : null}</td>
                  <td>{recipe ? recipe.carbohydrates : null}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="recipe-container__inside__lists">
            <div className="recipe-container__inside__lists__list">
              <div className="recipe-container__inside__lists__list__title">
                    Ingredients:
              </div>
              <ul>
                {recipe && recipe.ingredients ? recipe.ingredients.map(ingredient => (
                  <li key={ingredient.id}>{ingredient.ingredient}</li>
                )) : null}
              </ul>
            </div>
            <div className="recipe-container__inside__lists__list">
              <div className="recipe-container__inside__lists__list__title">
                    Instructions:
              </div>
              <ol>
                {recipe && recipe.instructions ? recipe.instructions.map(instruction => (
                  <li key={instruction.id}>{instruction.instruction}</li>
                )) : null}
              </ol>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RecipePresenter;
