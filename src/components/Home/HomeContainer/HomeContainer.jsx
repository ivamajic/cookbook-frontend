import React, { Component } from "react";

import HomePresenter from "../HomePresenter/HomePresenter";

class HomeContainer extends Component {
  render() {
    return (
      <HomePresenter />
    );
  }
}

export default HomeContainer;
