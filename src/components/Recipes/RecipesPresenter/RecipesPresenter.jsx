import React, { Component } from "react";

import "./RecipesPresenter.scss";

class RecipesPresenter extends Component {
  render() {
    return (
      <div className="recipes-container">
        <div className="recipes-container__header">
          <div className="recipes-container__header__title">{this.props.title}</div>
          <div className="recipes-container__header__search">
            <input type="text" value={this.props.searchTerm} placeholder="Filter recipes" onChange={e => this.props.filterRecipes(e.target.value)} />
            <select value={this.props.searchCategory} onChange={e => this.props.changeSearchCategory(e.target.value)}>
              <option value="name">By name</option>
              <option value="tag">By tag</option>
              <option value="author">By author</option>
            </select>
          </div>
        </div>
        <div className="recipes-container__inside">
          {this.props.recipes ? this.props.recipes.map(recipe => (
            <div className="recipes-container__inside__recipe" key={recipe.id}>
              <a href={`/recipe/${recipe.id}`}><img className="recipes-container__inside__recipe__image" src={recipe.imageUrl} alt="" /></a>
              <div className="recipes-container__inside__recipe__title">
                <a href={`/recipe/${recipe.id}`}>{recipe.title}</a>
              </div>
              <div className="recipes-container__inside__recipe__author">
                by
                {" "}
                {recipe.user[0].name}
              </div>
              <div className="recipes-container__inside__recipe__tags">
                {"("}
                {
                  recipe.tags.map(tag => (
                    <div style={{
                      fontWeight: this.props.searchCategory === "tag"
                      && this.props.searchTerm !== ""
                      && tag.tag.toUpperCase().includes(this.props.searchTerm.toUpperCase())
                        ? "bold"
                        : "normal",
                    }}
                    >
                      {tag.tag}
                    </div>
                  ))
                }
                {")"}
              </div>
            </div>
          )) : (<div>No recipes</div>)}
        </div>
      </div>
    );
  }
}

export default RecipesPresenter;
