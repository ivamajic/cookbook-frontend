import React, { Component } from "react";
import { connect } from "react-redux";
import { dispatchFetchRecipes, dispatchFetchMyRecipes } from "../../../redux/actions";

import RecipesPresenter from "../RecipesPresenter/RecipesPresenter";

class RecipesContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: "",
      searchCategory: "name",
      recipesToShow: [],
    };
    this.filterRecipes = this.filterRecipes.bind(this);
    this.changeSearchCategory = this.changeSearchCategory.bind(this);
  }

  componentDidMount() {
    if (this.props.location.pathname === "/managerecipes") {
      this.props.dispatch(dispatchFetchMyRecipes(this.props.loggedUser));
      this.setState({
        recipesToShow: this.props.myRecipes,
      });
    } else {
      this.props.dispatch(dispatchFetchRecipes());
      this.setState({
        recipesToShow: this.props.recipes,
      });
    }
  }

  filterRecipes = (searchTerm) => {
    this.setState({
      searchTerm,
    }, () => {
      const recipes = this.props.location.pathname === "/managerecipes" ? this.props.myRecipes : this.props.recipes;
      switch (this.state.searchCategory) {
        case "name": {
          this.setState({
            recipesToShow: recipes.filter(recipe => recipe.title.toUpperCase().includes(this.state.searchTerm.toUpperCase())),
          });
          break;
        }
        case "tag": {
          this.setState({
            recipesToShow: recipes.filter((recipe) => {
              let existsFlag = false;
              recipe.tags.forEach((tag) => {
                if (tag.tag.toUpperCase().includes(this.state.searchTerm.toUpperCase())) {
                  existsFlag = true;
                }
              });
              return existsFlag;
            }),
          });
          break;
        }
        case "author": {
          this.setState({
            recipesToShow: recipes.filter(recipe => recipe.user[0].name.toUpperCase().includes(this.state.searchTerm.toUpperCase())),
          });
          break;
        }
        default: {
          console.log("No default method");
        }
      }
    });
  }

  changeSearchCategory = (searchCategory) => {
    this.setState({
      searchCategory,
    }, () => {
      this.filterRecipes(this.state.searchTerm);
    });
  }

  render() {
    return (
      <RecipesPresenter
        title={this.props.location.pathname === "/managerecipes" ? "Manage recipes" : "recipes"}
        recipes={this.state.recipesToShow}
        searchTerm={this.state.searchTerm}
        searchCategory={this.state.searchCategory}
        filterRecipes={this.filterRecipes}
        changeSearchCategory={this.changeSearchCategory}
      />
    );
  }
}

const mapStateToProps = state => ({
  loggedUser: state.auth.user,
  recipes: state.recipes.recipes,
  myRecipes: state.recipes.myRecipes,
});

export default connect(mapStateToProps)(RecipesContainer);
