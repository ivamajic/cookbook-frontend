import React, { Component } from "react";

import "./NewRecipePresenter.scss";

class NewRecipePresenter extends Component {
  render() {
    return (
      <div className="new-recipe-container">
        <div className="new-recipe-container__header">
          <div className="new-recipe-container__header__title">{this.props.header}</div>
        </div>
        <div className="new-recipe-container__inside">
          <div className="new-recipe-container__inside__row">
            <input
              type="text"
              placeholder="Title"
              value={this.props.title}
              onChange={e => this.props.changeTitleInput(e.target.value)}
            />
          </div>
          <div className="new-recipe-container__inside__row">
            <input
              type="text"
              placeholder="Description"
              value={this.props.description}
              onChange={e => this.props.changeDescriptionInput(e.target.value)}
            />
          </div>
          <div className="new-recipe-container__inside__row">
            <input
              type="text"
              placeholder="Image URL"
              value={this.props.image}
              onChange={e => this.props.changeImageInput(e.target.value)}
            />
          </div>
          <div className="new-recipe-container__inside__row">
              Category:
            <select
              value={this.props.category}
              onChange={e => this.props.changeCategory(e.target.value)}
            >
              <option value="breakfast">Breakfast</option>
              <option value="lunch">Lunch</option>
              <option value="dinner">Dinner</option>
              <option value="dessert">Dessert</option>
            </select>
          </div>
          <div className="new-recipe-container__inside__row">
            <input
              type="number"
              placeholder="Calories"
              value={this.props.kcal}
              onChange={e => this.props.changeKcalInput(e.target.value)}
            />
          </div>
          <div className="new-recipe-container__inside__row">
            <input
              type="number"
              placeholder="Protein"
              value={this.props.protein}
              onChange={e => this.props.changeProteinInput(e.target.value)}
            />
          </div>
          <div className="new-recipe-container__inside__row">
            <input
              type="number"
              placeholder="Fat"
              value={this.props.fat}
              onChange={e => this.props.changeFatInput(e.target.value)}
            />
          </div>
          <div className="new-recipe-container__inside__row">
            <input
              type="number"
              placeholder="Carbohydrates"
              value={this.props.carbohydrates}
              onChange={e => this.props.changeCarbohydratesInput(e.target.value)}
            />
          </div>
          <div className="new-recipe-container__inside__row">
            <input
              type="text"
              placeholder="Add an ingredient"
              value={this.props.ingredient}
              onChange={e => this.props.changeIngredientInput(e.target.value)}
              onKeyUp={e => this.props.addIngredient(e)}
            />
          </div>
          <div className="new-recipe-container__inside__row flex">
            {
                this.props.ingredients.map(ingredient => <div>{ingredient}</div>)
            }
          </div>
          <div className="new-recipe-container__inside__row">
            <input
              type="text"
              placeholder="Add an instruction"
              value={this.props.instruction}
              onChange={e => this.props.changeInstructionInput(e.target.value)}
              onKeyUp={e => this.props.addInstruction(e)}
            />
          </div>
          <div className="new-recipe-container__inside__row flex">
            {
                this.props.instructions.map(instruction => <div>{instruction}</div>)
            }
          </div>
          <div className="new-recipe-container__inside__row">
            <input
              type="text"
              placeholder="Add a tag"
              value={this.props.tag}
              onChange={e => this.props.changeTagInput(e.target.value)}
              onKeyUp={e => this.props.addTag(e)}
            />
          </div>
          <div className="new-recipe-container__inside__row flex">
            {
                this.props.tags.map(tag => <div>{tag}</div>)
            }
          </div>
          <div className="new-recipe-container__inside__row">
            <button type="submit" onClick={this.props.submit}>Submit</button>
          </div>
        </div>
      </div>
    );
  }
}

export default NewRecipePresenter;
