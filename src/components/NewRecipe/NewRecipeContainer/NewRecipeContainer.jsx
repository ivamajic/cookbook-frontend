import React, { Component } from "react";
import { connect } from "react-redux";

import NewRecipePresenter from "../NewRecipePresenter/NewRecipePresenter";
import { dispatchCreateRecipe, dispatchFetchRecipeById, dispatchEditRecipe } from "../../../redux/actions";

class NewRecipeContainer extends Component {
  constructor(props) {
    super(props);
    this.changeTitleInput = this.changeTitleInput.bind(this);
    this.changeDescriptionInput = this.changeDescriptionInput.bind(this);
    this.changeImageInput = this.changeImageInput.bind(this);
    this.changeKcalInput = this.changeKcalInput.bind(this);
    this.changeProteinInput = this.changeProteinInput.bind(this);
    this.changeFatInput = this.changeFatInput.bind(this);
    this.changeCarbohydratesInput = this.changeCarbohydratesInput.bind(this);
    this.addIngredient = this.addIngredient.bind(this);
    this.addIngredient = this.addIngredient.bind(this);
    this.addTag = this.addTag.bind(this);
    this.changeCategory = this.changeCategory.bind(this);
    this.submit = this.submit.bind(this);
    this.changeIngredientInput = this.changeIngredientInput.bind(this);
    this.changeInstructionInput = this.changeInstructionInput.bind(this);
    this.changeTagInput = this.changeTagInput.bind(this);

    this.state = {
      title: "",
      description: "",
      imageUrl: "",
      kcal: "",
      protein: "",
      fat: "",
      carbohydrates: "",
      ingredient: "",
      ingredients: [],
      instruction: "",
      instructions: [],
      tag: "",
      tags: [],
      category: "breakfast",
    };
  }

  componentDidMount() {
    if (this.props.location.pathname.includes("editrecipe")) {
      this.props.dispatch(dispatchFetchRecipeById(this.props.match.params.id));
      console.log(this.props.selectedRecipe);
      this.setState({
        title: this.props.selectedRecipe.title,
        description: this.props.selectedRecipe.description,
        imageUrl: this.props.selectedRecipe.imageUrl,
        kcal: this.props.selectedRecipe.kcal,
        protein: this.props.selectedRecipe.protein,
        carbohydrates: this.props.selectedRecipe.carbohydrates,
        fat: this.props.selectedRecipe.fat,
        category: this.props.selectedRecipe.category,
        ingredients: this.props.selectedRecipe.ingredients.map(ingredient => ingredient.ingredient),
        instructions: this.props.selectedRecipe.instructions.map(instruction => instruction.instruction),
        tags: this.props.selectedRecipe.tags.map(tag => tag.tag),
      });
    }
  }

  changeTitleInput = (value) => {
    this.setState({
      title: value,
    });
  }

  changeDescriptionInput = (value) => {
    this.setState({
      description: value,
    });
  }

  changeImageInput = (value) => {
    this.setState({
      imageUrl: value,
    });
  }

  changeKcalInput = (value) => {
    this.setState({
      kcal: value,
    });
  }

  changeProteinInput = (value) => {
    this.setState({
      protein: value,
    });
  }

  changeFatInput = (value) => {
    this.setState({
      fat: value,
    });
  }

  changeCarbohydratesInput = (value) => {
    this.setState({
      carbohydrates: value,
    });
  }

  changeIngredientInput = (value) => {
    this.setState({
      ingredient: value,
    });
  }

  changeInstructionInput = (value) => {
    this.setState({
      instruction: value,
    });
  }

  changeTagInput = (value) => {
    this.setState({
      tag: value,
    });
  }

  addIngredient = (e) => {
    if (e.key === "Enter") {
      const { ingredients } = this.state;
      ingredients.push(
        e.target.value,
      );
      this.setState({
        ingredients,
        ingredient: "",
      });
    }
  }

  addInstruction = (e) => {
    if (e.key === "Enter") {
      const { instructions } = this.state;
      instructions.push(
        e.target.value,
      );
      this.setState({
        instructions,
        instruction: "",
      });
    } else {
      this.setState({
        instruction: e.target.value,
      });
    }
  }

addTag = (e) => {
  if (e.key === "Enter") {
    const { tags } = this.state;
    tags.push(
      e.target.value,
    );
    this.setState({
      tags,
      tag: "",
    });
  } else {
    this.setState({
      tag: e.target.value,
    });
  }
}

changeCategory = (value) => {
  this.setState({
    category: value,
  });
}

submit = () => {
  if (this.props.location.pathname.includes("editrecipe")) {
    this.props.dispatch(dispatchEditRecipe(
      this.state.title,
      this.state.description,
      this.state.imageUrl,
      Number(this.state.kcal),
      Number(this.state.protein),
      Number(this.state.fat),
      Number(this.state.carbohydrates),
      this.state.category,
      this.state.ingredients,
      this.state.instructions,
      this.state.tags,
      this.props.loggedUser.accessToken,
      this.props.selectedRecipe.id,
    ));
  } else {
    this.props.dispatch(dispatchCreateRecipe(
      this.state.title,
      this.state.description,
      this.state.imageUrl,
      Number(this.state.kcal),
      Number(this.state.protein),
      Number(this.state.fat),
      Number(this.state.carbohydrates),
      this.state.category,
      this.props.loggedUser.id,
      this.state.ingredients,
      this.state.instructions,
      this.state.tags,
      this.props.loggedUser.accessToken,
    ));
  }
}

render() {
  return (
    <NewRecipePresenter
      header={this.props.location.pathname.includes("editrecipe") ? `Edit ${this.state.title}` : "New recipe"}
      title={this.state.title}
      changeTitleInput={this.changeTitleInput}
      description={this.state.description}
      changeDescriptionInput={this.changeDescriptionInput}
      image={this.state.imageUrl}
      changeImageInput={this.changeImageInput}
      kcal={this.state.kcal}
      changeKcalInput={this.changeKcalInput}
      protein={this.state.protein}
      changeProteinInput={this.changeProteinInput}
      fat={this.state.fat}
      changeFatInput={this.changeFatInput}
      carbohydrates={this.state.carbohydrates}
      changeCarbohydratesInput={this.changeCarbohydratesInput}
      ingredient={this.state.ingredient}
      ingredients={this.state.ingredients}
      addIngredient={this.addIngredient}
      instruction={this.state.instruction}
      instructions={this.state.instructions}
      addInstruction={this.addInstruction}
      tag={this.state.tag}
      tags={this.state.tags}
      addTag={this.addTag}
      submit={this.submit}
      changeCategory={this.changeCategory}
      category={this.state.category}
      changeIngredientInput={this.changeIngredientInput}
      changeInstructionInput={this.changeInstructionInput}
      changeTagInput={this.changeTagInput}
    />
  );
}
}

const mapStateToProps = state => ({
  loggedUser: state.auth.user,
  selectedRecipe: state.recipes.selectedRecipe,
});

export default connect(mapStateToProps)(NewRecipeContainer);
